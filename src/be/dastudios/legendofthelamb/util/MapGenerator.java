package be.dastudios.legendofthelamb.util;
import be.dastudios.legendofthelamb.area.BasicLandscapeCel;
import be.dastudios.legendofthelamb.area.MapCel;
import be.dastudios.legendofthelamb.area.MapCelItems;
import be.dastudios.legendofthelamb.area.MovementDirections;

import java.util.*;

public class MapGenerator {
    private static Random randomGenerator = new Random();

    public MapGenerator() {
    }

    public static void generateLandscape(MapCel[][] areaMap, List<MapCel> listOfLandscapeElements) {
        Collections.shuffle(listOfLandscapeElements);
        for (int i = 0; i < areaMap.length-1; i++) {
            for (int j = 0; j < areaMap[i].length-1; j++) {
                if (areaMap[i][j] == null && (areaMap[i][j +1] != null || areaMap[i+1][j ] != null)) {
                    areaMap[i][j] = listOfLandscapeElements.get(0);
                } else if (!listOfLandscapeElements.contains(areaMap[i][j])) {
                    if (i == 0) {
                        i = areaMap.length-1;
                        if (j == 0) {
                            j = areaMap[i].length - 1;
                            if (areaMap[i][j] == null && ((areaMap[i][j - 1] != null && !listOfLandscapeElements.contains(areaMap[i][j - 1])) || (areaMap[i - 1][j] != null && !listOfLandscapeElements.contains(areaMap[i - 1][j])))) {
                                areaMap[i][j] = listOfLandscapeElements.get(0);
                            }
                            j = 0;
                        }
                        i = 0;

                    } else if (j == 0) {
                        j = areaMap[i].length - 1;
                        if (areaMap[i][j] == null && (areaMap[i][j - 1] != null && !listOfLandscapeElements.contains(areaMap[i][j - 1]) || (areaMap[i - 1][j] != null && !listOfLandscapeElements.contains(areaMap[i - 1][j])))) {
                            areaMap[i][j] = listOfLandscapeElements.get(0);
                        }
                        j = 0;
                    }else {
                        if (areaMap[i][j] == null && (areaMap[i][j - 1] != null && !listOfLandscapeElements.contains(areaMap[i][j - 1]) || areaMap[i - 1][j] != null && !listOfLandscapeElements.contains(areaMap[i - 1][j]))) {
                            areaMap[i][j] = listOfLandscapeElements.get(0);
                        }
                    }

                }
                if (j % 3 == 0) {
                    Collections.shuffle(listOfLandscapeElements);
                }
            }
        }
    }
    public static MapCel[][] generateMap() {
        return generateMap(40, 40);
    }

    public static MapCel[][] generateMap(int areaLength, int areaWidth) {
        MapCel[][] areaMap = new MapCel[areaLength][areaWidth];
        String defaultMap = ";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;\n" +
                ";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;\n" +
                ";;VVVVVVVVVVVVVVVVXXXXXXXXXXXXXXXXX;;;;;\n" +
                ";;V;;;;;;;;;;;;;;;X;;;;;;;;;;;;;;;X;;;;;\n" +
                ";;V;;;;;;;;;;;;;;;X;;;;;;;;;;;;;;;X;;;;;\n" +
                ";;V;;;;;;;;;;;;;;;X;;;;;;;;;;;;;;;X;;;;;\n" +
                ";;V;;;;;;;;;;;;;;;X;;;;;;;;;;;;;;;X;;;;;\n" +
                ";;V;;;;;;;;;;;;;;;X;;;;;;;;;;;;;;;X;;;;;\n" +
                ";;V;;;;;;;;;;;;;;;X;;;;;;;;;;;;;;;X;;;;;\n" +
                ";;V;;;;;;;;;;;;;;;X;;;;;;;;;;;;;;;X;;;;;\n" +
                ";;V;;;;;;;;;;;;;;;X;;;;;;;;;;;;;;;X;;;;;\n" +
                ";;K;;;;;;;;;;;;;;;X;;;;;;;;;;;;;;;X;;;;;\n" +
                ";;VVVVVVVVVVVVVVVVX;;;;;;;;;;;;;;;X;;;;;\n" +
                ";;K;;;;;;;;;;;;;;;XXXXXXXXD;;;;;;;X;;;;;\n" +
                ";;V;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;X;;;;;\n" +
                ";;V;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;X;;;;;\n" +
                ";;V;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;X;;;;;\n" +
                ";;V;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;X;;;;;\n" +
                ";;V;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;X;;;;;\n" +
                ";;V;;;;;;;;;;;;;VVVVVVVVVVVVW;;;;;X;;;;;\n" +
                ";;V;;;;;;;;;;;;;V;;;;;;;;;;;;;;;;;X;;;;;\n" +
                ";XXXXXXXXXXX;;;;V;;;;;;;;;;;;;;;;;X;;;;;\n" +
                ";X;;;;;;;;;X;;;;V;;;;;;;;;;;;;;;;;X;;;;;\n" +
                ";X;;;;;;;;;XVVVVV;;;;;;;;;;;;;;;;;X;;;;;\n" +
                ";X;;;;;;;;;X;;;;;;;;;;;;;;;;;;;;;;X;;;;;\n" +
                ";X;;;;;;;;;X;;;;;;XXXXX;;;;;;;;;;;X;;;;;\n" +
                ";X;;;;;;;;;X;;;;;;X;;;X;;;;;;;;;;;X;;;;;\n" +
                ";X;;;;;;;;;X;;;;;;X;;;X;;;;;;;;;;;X;;;;;\n" +
                ";X;;;;;;;;;X;;;;;;X;;;X;;;;;;;;;;;X;;;;;\n" +
                ";X;;;;;;;;;XXXXXXXX;;;X;;;;;;;;;;;X;;;;;\n" +
                ";X;;;;;;;;;;;;;;;;;;;;X;;;;;XXXXXXX;;;;;\n" +
                ";X;;;;;;;;;;;;;;;;;;;;X;;;;;X;;;V;;;;;;;\n" +
                ";X;;;;;;;;;;;;;;;;;;;;X;;;;;X;;;VVVVVVV;\n" +
                ";X;;;;;;;;;;;;;;;;;;;;X;;;;;X;;;V;;;;;V;\n" +
                ";XXXXXXXX;;;;;;;;XXXXXX;;;;;X;;;V;;;;;V;\n" +
                ";;;;;;;;X;;;;;;;;X;;;;;;;;;;X;VVV;;;;;V;\n" +
                ";;;;;;;;X;;;;;;;;XXXXXXXXXXXX;V;;;;;;;V;\n" +
                ";;;;;;XXX;;;;;;;;;;;;;;;;;;;;;VVVVVVVVV;\n" +
                ";;;;;;X;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;\n" +
                ";;;;;;S;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;";
        char[][] mapArea = new char[40][40];
        String[] mapRow = defaultMap.split("\n");
        for (int i = 0; i < 40; i++) {
            mapArea[i] = mapRow[i].toCharArray();
        }
        for (int i = 0; i < 40; i++) {
            for (int j = 0; j < 40; j++) {
                if (mapArea[i][j] != ';' && mapArea[i][j] != 0) {
                    MapCel cel = new MapCel("pad", new int[]{i, j});
                    switch (mapArea[i][j]) {
                        case 'S':
                            cel.addCelElements(new MapCelItems("start flag"));
                            break;
                        case 'D':
                            cel.addCelElements(new MapCelItems("end flag"));
                            break;
                        case 'X':
                            break;
                        case 'V':
                            cel.setDescription("small pad");
                            break;
                        case 'K':
                            cel.addCelElements(new MapCelItems("door", true, false, false));
                            break;
                        case 'W':
                            cel.addCelElements(new MapCelItems("Bugbear leader", true));
                            cel.addCelElements(new MapCelItems("door", false, true,true));
                            break;
                    }
                    for (MovementDirections value : MovementDirections.values()) {
                        int[] k = value.calculateDirectionCoordinate(new int[]{i, j});
                        if (k[0] > -1 && k[1] > -1 && k[0] < 40 && k[1] < 40) {
                            if (mapArea[k[0]][k[1]] == ';' && areaMap[k[0]][k[1]] == null) {
                                areaMap[k[0]][k[1]] = new MapCel(BasicLandscapeCel.values()[new Random().nextInt(4)].description(), k);
                            }
                        }
                        areaMap[i][j] = cel;
                    }

                }
            }
        }
        for (MapCel[] areaObject : areaMap) {
            for (MapCel mapCel : areaObject) {
                if (mapCel != null && (mapCel.toString().equals("small pad") || mapCel.toString().equals("pad"))) {
                    for (MovementDirections value : MovementDirections.values()) {
                        int[] coord = value.calculateDirectionCoordinate(mapCel.getCelCoordinates());
                        if (coord[0] > -1 && coord[1] > -1 && coord[0] < 40 && coord[1] < 40) {
                            mapCel.addCelNeighbour(value,areaMap[coord[0]][coord[1]]);
                        }

                    }
                }

            }
        }
        return areaMap;
    }
    private static int[] setRandomStart(int areaLength, int areaWidth) {
        int[] startCoordination = new int[2];
        int selectSide = randomGenerator.nextInt(4);
        switch (selectSide) {
            case 0:
                startCoordination[0] = randomGenerator.nextInt(areaLength);
                startCoordination[1] = 0;
            case 1:
                startCoordination[0] = areaLength;
                startCoordination[1] = randomGenerator.nextInt(areaWidth);
            case 2:
                startCoordination[0] = randomGenerator.nextInt(areaLength);
                startCoordination[1] = areaWidth;
            case 3:
                startCoordination[0] = 0;
                startCoordination[1] = randomGenerator.nextInt(areaWidth);
        }
        return startCoordination;
    }
    private static int[] generateRandomCoordinates(MapCel[][] areaMap, int areaLength, int areaWidth) {
        int[] coordinate = {randomGenerator.nextInt(areaLength-6)+3,randomGenerator.nextInt(areaWidth-6)+3};
        for (int i = coordinate[0]-2; i < coordinate[0]+3; i++) {
            for (int j = coordinate[1]-2; j < coordinate[1]+3; j++) {
                if (areaMap[i][j] != null) {
                    return generateRandomCoordinates(areaMap,areaLength,areaWidth);
                }
            }
        }
        return coordinate;
    }
    private static void createCrucialPad(MapCel[][] mapArea) {

    }
}
