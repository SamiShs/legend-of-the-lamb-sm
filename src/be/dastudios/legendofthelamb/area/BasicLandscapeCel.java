package be.dastudios.legendofthelamb.area;

public enum BasicLandscapeCel {
    F{
        @Override
        public String description() {
            return "forest";
        }
    },
    R{
        @Override
        public String description() {
            return "river";
        }
    },
    X{
        @Override
        public String description() {
            return "mountain";
        }
    },
    D{
        @Override
        public String description() {
            return "canyon";
        }
    };

    public String description() {
        return "default";
    }
    @Override
    public String toString() {
        return description();
    }
}
