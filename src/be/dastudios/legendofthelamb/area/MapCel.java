package be.dastudios.legendofthelamb.area;

import be.dastudios.legendofthelamb.characters.Creature;
import be.dastudios.legendofthelamb.characters.monsters.Monster;

import java.io.Serializable;
import java.util.*;

public class MapCel implements Serializable {

    private String description;
    private int[] celCoordinates;
    private List<MapCelItems> celElements;
    private List<Creature> celCreatures;
    private Map<MovementDirections, MapCel> celNeighbours = new HashMap<>();

    public MapCel(String description, int[] celCoordinates) {
        this.description = description;
        this.celCoordinates = celCoordinates;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void addCelElements(MapCelItems element) {
        if (celElements == null) {
            celElements = new ArrayList<>();
        }
        celElements.add(element);
    }

    public List<MapCelItems> getCelElements() {
        if (celElements == null) {
            return new ArrayList<>();
        }
        return celElements;
    }

    public void addCreatureToCel(Creature creature) {
        if (celCreatures == null) {
            celCreatures = new ArrayList<>();
        }
        celCreatures.add(creature);
    }

    public void removeCreature(Creature creature) {
        celCreatures.remove(creature);
    }
    public boolean hasMonsters() {
        for (Creature celCreature : celCreatures) {
            if (celCreature instanceof Monster) {
                return true;
            }
        }
        return false;
    }

    public List<Creature> getCreatures() {
        if (celCreatures == null) {
            return new ArrayList<>();
        }
        return celCreatures;
    }


    public void addCelNeighbour(MovementDirections movementDirections, MapCel neighbour) {
        celNeighbours.putIfAbsent(movementDirections, neighbour);
    }

    public MapCel getNeighbourCrucial(MapCel previous) {
        for (MovementDirections value : MovementDirections.values()) {
            if (getNeighbour(value).description.equals("pad") && getNeighbour(value).getCelCoordinates() != previous.getCelCoordinates()) {
                return getNeighbour(value);
            }
        }
        return null;
    }

    public MapCel getNeighbour(MovementDirections movementDirections) {
        if (celNeighbours.containsKey(movementDirections) && celNeighbours.get(movementDirections) != null) {
            return celNeighbours.get(movementDirections);
        } else {
            return null;
        }
    }

    public boolean unlockCelElement(MapCelItems key) {
        for (int i = 0; i < celElements.size(); i++) {
            if (celElements.get(i).isDeobstacle() && celElements.get(i).tryKey(key)) {
                celElements.get(i).deObstacle(celElements.get(i));
            }
        }
        return false;
    }

    public int[] getCelCoordinates() {
        return celCoordinates;
    }

    @Override
    public String toString() {
        return description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MapCel mapCel = (MapCel) o;
        return description.equals(mapCel.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(description);
    }


}
