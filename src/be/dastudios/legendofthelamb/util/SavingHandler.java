package be.dastudios.legendofthelamb.util;

import be.dastudios.legendofthelamb.area.MainMap;
import be.dastudios.legendofthelamb.characters.Player;

import java.io.Serializable;

public class SavingHandler implements Serializable {
    private Player[] player;
    private MainMap mainMap;
    private String saveFileName;

    public SavingHandler() {

    }

    public SavingHandler(Player[] player, MainMap mainMap, String saveFileName) {
        this.player = player;
        this.mainMap = mainMap;
        this.saveFileName = saveFileName;
    }

    public String getSaveFileName() {
        return saveFileName;
    }

    public Player[] getPlayer() {
        return this.player;
    }

    public MainMap getMainMap() {
        return this.mainMap;
    }
}
