package be.dastudios.legendofthelamb.characters;

import be.dastudios.legendofthelamb.util.Dice;

import java.io.Serializable;

public interface Creature extends Serializable {
    int getAC();
    int getCurrentHP();
    int getMaxHP();
    void setCurrentHP(int currentHP);
    int getAttacksAmount();
    default int getInitiative(){
        return Dice.throwDice(20);
    }
    String getName();
}
