package be.dastudios.legendofthelamb.area;

import java.io.Serializable;
import java.util.Objects;

public class MapCelItems implements Serializable {
    private String Description;
    private boolean isObstacle;
    private boolean isDeobstacle;
    private boolean isCollectable = false;

    public MapCelItems(String description) {
        this(description, false);
    }

    public MapCelItems(String description, boolean isObstacle) {
        this(description, isObstacle, false);
    }

    public MapCelItems(String description, boolean isObstacle, boolean isDeobstacle) {
        this(description, isObstacle, isDeobstacle, isDeobstacle);
    }

    public MapCelItems(String description, boolean isObstacle, boolean isDeobstacle, boolean isCollectable) {
        this.isCollectable = isCollectable;
        Description = description;
        this.isObstacle = isObstacle;
        this.isDeobstacle = isDeobstacle;
    }

    public String getDescription() {
        return Description;
    }

    public boolean isObstacle() {
        return isObstacle;
    }

    public boolean isDeobstacle() {
        return isDeobstacle;
    }

    public boolean tryKey(MapCelItems key) {
        return key.Description == this.getDescription();
    }

    public void deObstacle(MapCelItems mapCelItems) {
        this.isObstacle = false;
        this.Description = ("unlocked " + this.getDescription());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MapCelItems that = (MapCelItems) o;
        return isCollectable == that.isCollectable &&
                Objects.equals(Description, that.Description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(Description, isCollectable);
    }
}
