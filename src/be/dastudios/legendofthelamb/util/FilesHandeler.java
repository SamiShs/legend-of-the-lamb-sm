package be.dastudios.legendofthelamb.util;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

public class FilesHandeler {
    public static <E> E loadFile(E type, String extention) {
        return loadFile(type, extention, "");
    }

    public static <E> E loadFile(E type, String extention,String filename) {
        File folder = Paths.get("").toAbsolutePath().toFile();
        File[] files = folder.listFiles((File f,String s)->s.contains(extention));
        if (extention.contains("map") && files.length < 1) {
            return (E) MapGenerator.generateMap();
        }
        E loadedFiles = null;
        int input =0;
        if (filename.isEmpty()) {
            input = Keyboard.chooseOption(files, "choose the " + extention.replace(',', ' ') + " file to load");
        } else {
            for (int i = 0; i < files.length; i++) {
                if (files[i].getPath().contains(filename + extention)) {
                    input = i;
                    break;
                }
            }
        }

        try (FileInputStream inputStream = new FileInputStream(files[input].getPath());
             ObjectInputStream objectLoader = new ObjectInputStream(inputStream)) {
            loadedFiles = (E)objectLoader.readObject();

        } catch (Exception ioe) {
            ioe.printStackTrace();
        }
        return loadedFiles;
    }

    public static <E> void writeFile(E file) {
        String name = Keyboard.readString("give in a file name");
        writeFile(file,name);
    }


    public static <E > void writeFile (E file, String name){
        Path path = Paths.get("");
        try (FileOutputStream mapwriter = new FileOutputStream(path.resolve(name).toFile());
             ObjectOutputStream objectOutputStream = new ObjectOutputStream(mapwriter)
        ) {
            objectOutputStream.writeObject(file);
            System.out.println(name + " is saved.");
        } catch (IOException ioe) {

        }

    }

    public static void save(SavingHandler savingHandler) {
        writeFile(savingHandler,savingHandler.getSaveFileName()+".save");
    }

    public static SavingHandler loadSave(SavingHandler savingHandler) {
        return loadSave(savingHandler, "");
    }

    public static SavingHandler loadSave(SavingHandler savingHandler, String savingFileName) {
        if (savingHandler == null && savingFileName.isEmpty()) {
            return loadFile(new SavingHandler(), ".save");
        } else {
            return loadFile(savingHandler, ".save", savingHandler.getSaveFileName());
        }


    }

}
