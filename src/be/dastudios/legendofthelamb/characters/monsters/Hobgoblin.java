package be.dastudios.legendofthelamb.characters.monsters;

import be.dastudios.legendofthelamb.util.Dice;

public class Hobgoblin implements Monster{
    private int AC = 18;
    private int maxHP = 25;
    private int currentHP = 25;
    private CR cr = CR.CR2;
    private int attack;
    private int damage;
    private String name = "Hobgoblin";

    @Override
    public String getName() {
        return name;
    }

    public int getAC() {
        return AC;
    }

    public int getMaxHP() {
        return maxHP;
    }

    public int getCurrentHP() {
        return currentHP;
    }

    public void setCurrentHP(int currentHP) {
        if(currentHP>maxHP){
            this.currentHP = maxHP;
        }else{
            this.currentHP = currentHP;
        }

    }

    public int getCR() {
        return 2;
    }
    public int  getXPfromCR() {
        return cr.getXp();
    }

    public int getAttack() {
        return Dice.throwDice(20) + 4;
    }

    public int getDamage() {
        return Dice.throwDice(8) + 3;
    }

    @Override
    public String toString(){
        return String.format("Troll{ AC = %d, maxHP = %d, currentHP = %d, CR = %d }", AC, maxHP, currentHP, getCR());
    }
}
