package be.dastudios.legendofthelamb.characters.professions;

import be.dastudios.legendofthelamb.characters.Creature;
import be.dastudios.legendofthelamb.characters.Player;
import be.dastudios.legendofthelamb.characters.items.Armour;
import be.dastudios.legendofthelamb.characters.items.Weapon;
import be.dastudios.legendofthelamb.characters.monsters.Monster;
import be.dastudios.legendofthelamb.util.Dice;

import java.util.HashMap;
import java.util.Map;

public class Healer implements Profession{
    private Map<String, Integer> abilities = new HashMap<>();
    private int maxLifeDice = 6;
    private int armourBase = 4;
    private Weapon weapon;
    private Armour armour;

    public Healer(){
        setMap();
        setWeapon(Weapon.HOLY_SYMBOL);
        setArmour(Armour.CLOTHES);
    }

    private void setMap(){
        abilities.put("Strength", 8);
        abilities.put("Constitution", 14);
        abilities.put("Dexterity", 11);
        abilities.put("Wisdom", 18);
        abilities.put("Intelligence", 13);
        abilities.put("Charisma", 15);
    }

    @Override
    public Map<String, Integer> getAbilities() {
        return abilities;
    }

    public int getMaxLifeDice() {
        return maxLifeDice;
    }

    public int getArmourBase(){
        return armourBase;
    }

    public void level1Power(Player player, Creature creature){
        if(getWeapon()==Weapon.HOLY_SYMBOL){
            if(creature instanceof Player){
                int healing = weapon.getDamage(player);
                creature.setCurrentHP(creature.getCurrentHP()+healing);
                System.out.printf("%s heals %s (maxHP = %d) for %d: currentHP = %d\n", player.getName(), creature.getName(), creature.getMaxHP(), healing, creature.getCurrentHP());
            }else if(creature instanceof Monster){
                int tryAttack = weapon.getAttack(player);
                System.out.printf("%s attacks %s (AC = %d) for %d\n", player.getName(), creature.getName(), creature.getAC(), tryAttack);
                if(tryAttack>=creature.getAC()){
                    System.out.println("Hit succesful!");
                    int damage = weapon.getDamage(player);
                    creature.setCurrentHP(creature.getCurrentHP()-damage);
                    System.out.printf("%s wounds %s (maxHP = %d) for %d: currentHP = %d\n", player.getName(), creature.getName(), creature.getMaxHP(), damage, creature.getCurrentHP());
                }else{
                    System.out.println("Hit failed!");
                }
            }
        }else{
            System.out.println("You can only cast spells with a Holy Symbol equipped!");
        }

    }

    public void level3Power(Player player, Creature creature){
        if(getWeapon()==Weapon.HOLY_SYMBOL) {
            if (creature instanceof Player) {
                int healing = Dice.throwDice(12) + (player.getAbilities().get("Wisdom")-10)/2;
                creature.setCurrentHP(creature.getCurrentHP() + healing);
                System.out.printf("%s heals %s (maxHP = %d) for %d: currentHP = %d\n", player.getName(), creature.getName(), creature.getMaxHP(), healing, creature.getCurrentHP());
            }
        }else{
            System.out.println("You can only cast spells with a Holy Symbol equipped!");
        }
    }


    public void level4Power(Player player, Creature creature) {
        if(getWeapon()==Weapon.HOLY_SYMBOL){
            if(creature instanceof Monster){
                System.out.println("Let another creature despair and inflict damage on itself.");
                int tryAttack = getWeapon().getAttack(player);
                System.out.printf("%s attacks %s (Intelligence = %d) for %d\n", player.getName(), creature.getName(), ((Monster)creature).getCR()*5, tryAttack);
                if(tryAttack>=((Monster) creature).getCR()*5){
                    System.out.println("Hit succesful!");
                    int damage = ((Monster) creature).getDamage();
                    creature.setCurrentHP(creature.getCurrentHP()-damage);
                    System.out.printf("%s wounds %s (maxHP = %d) for %d: currentHP = %d\n", creature.getName(), creature.getName(), creature.getMaxHP(), damage, creature.getCurrentHP());
                }else{
                    System.out.println("Hit failed!");
                }
            }
        }else{
            System.out.println("You can only cast spells with a Holy Symbol equipped!");
        }
    }

    public void level5Power(Player player, Creature creature){
        if(getWeapon()==Weapon.HOLY_SYMBOL){
            if(creature instanceof Player){
                int healing = Dice.throwDice(12) + Dice.throwDice(12) + (player.getAbilities().get("Wisdom")-10)/2;
                creature.setCurrentHP(creature.getCurrentHP()+healing);
                System.out.printf("%s heals %s (maxHP = %d) for %d: currentHP = %d\n", player.getName(), creature.getName(), creature.getMaxHP(), healing, creature.getCurrentHP());
            }else if(creature instanceof Monster){
                int damage = Dice.throwDice(12) + Dice.throwDice(12) + (player.getAbilities().get("Wisdom")-10)/2;
                creature.setCurrentHP(creature.getCurrentHP()-damage);
                System.out.printf("%s wounds %s (maxHP = %d) for %d: currentHP = %d\n", player.getName(), creature.getName(), creature.getMaxHP(), damage, creature.getCurrentHP());
            }
        }else{
            System.out.println("You can only cast spells with a Holy Symbol equipped!");
        }
    }

    public void setWeapon(Weapon weapon){
        this.weapon = weapon;
    }

    @Override
    public Armour getArmour() {
        return armour;
    }

    @Override
    public void setArmour(Armour armour) {
        this.armour = armour;
    }

    @Override
    public Weapon getWeapon() {
        return weapon;
    }
}
