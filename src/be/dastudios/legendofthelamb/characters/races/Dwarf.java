package be.dastudios.legendofthelamb.characters.races;

import java.util.HashMap;
import java.util.Map;

public class Dwarf implements Race {
    private Map<String, Integer> abilities = new HashMap<>();
    private int baseSpeed = 5;

    public Dwarf() {
        setMap();
    }

    private void setMap(){
        abilities.put("Strength", 2);
        abilities.put("Constitution", 2);
        abilities.put("Dexterity", 0);
        abilities.put("Wisdom", 0);
        abilities.put("Intelligence", 0);
        abilities.put("Charisma", 0);
    }

    public Map<String, Integer> getAbilities(){
        return abilities;
    }

    public int getBaseSpeed() {
        return baseSpeed;
    }
}
