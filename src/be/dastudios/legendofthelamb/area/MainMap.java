package be.dastudios.legendofthelamb.area;

import be.dastudios.legendofthelamb.characters.Creature;
import be.dastudios.legendofthelamb.characters.Player;
import be.dastudios.legendofthelamb.characters.monsters.*;
import be.dastudios.legendofthelamb.util.ExceptionHandler;
import be.dastudios.legendofthelamb.util.FilesHandeler;
import be.dastudios.legendofthelamb.util.MapGenerator;

import java.io.Serializable;
import java.util.*;

public class MainMap implements Serializable {

    private MapCel currentCel;
    private List<MapCelItems> collectedItemList;
    private Set<String> disabledCels;
    private MapCel[][] mainMap;
    private MapGenerator mapGenerator;
    private Map<String, Creature> creaturesList = new HashMap<>();

    public MainMap() {
    }

    private void loadMaps() {
        mainMap = FilesHandeler.loadFile(mainMap, ".map");
        setStartcel();
        for (BasicLandscapeCel value : BasicLandscapeCel.values()) {
            addDisabledCel(value.description());
        }
    }

    public void startJourneyFor(Player player, int population, int monsterPerCel) {
        loadMaps();
        creaturesList.put("player", player);
        creaturesList.put("monster1", new Bugbear());
        creaturesList.put("monster2", new GoblinFighter());
        creaturesList.put("monster3", new GoblinMinion());
        creaturesList.put("monster4", new GoblinRanger());
        creaturesList.put("monster5", new Wolf());
        creaturesList.put("monster6", new Troll());
        creaturesList.put("monster7", new Hobgoblin());
        currentCel.addCreatureToCel(player);
        for (int i = 0; i < monsterPerCel; i++) {
            generateMonstersOnMap((population/monsterPerCel)*2);
        }

        describeSurrounding();
    }

    public void addPlayer(Player playerSlave) {
        creaturesList.put(playerSlave.getName() + "slave", playerSlave);
        currentCel.addCreatureToCel(playerSlave);
    }

    private void setStartcel() {
        boolean found = false;
        for (MapCel[] mapCels : mainMap) {
            for (int i = 0; i < mapCels.length; i++) {
                if (mapCels[i] != null) {
                    MapCel mapCel = mapCels[i];
                    if (mapCel.getCelElements() != null && !mapCel.getCelElements().isEmpty() && mapCel.getCelElements().get(0).getDescription().equals("start flag")) {
                        currentCel = mapCel;
                        found = true;
                        break;
                    }
                }
            }
            if (found) {
                break;
            }
        }
    }

    private void generateMonstersOnMap(int creaturePopulation) {
        MapCel tempsel = currentCel;
        MapCel tempsel1 = currentCel;
        while (creaturePopulation > 0) {
            int rand = new Random().nextInt(7);
            for (int i = 0; i < rand + 2; i++) {
                MapCel tempse2 = tempsel;
                tempsel = tempsel.getNeighbourCrucial(tempsel1);
                tempsel1 = tempse2;
            }
            for (Creature value : creaturesList.values()) {
                if (value instanceof Monster && ((Monster) value).getMaxHP() < ((Player) creaturesList.get("player")).getMaxHP()) {
                    tempsel.addCreatureToCel(createMonsterByType((Monster) value));
                    creaturePopulation--;
                    break;
                }
            }
        }
    }

    private <E extends Monster> Monster createMonsterByType(E monster) {
        if (monster instanceof Bugbear) {
            return new Bugbear();
        } else if (monster instanceof GoblinFighter) {
            return new GoblinFighter();
        } else if (monster instanceof GoblinMinion) {
            return new GoblinMinion();
        } else if (monster instanceof GoblinRanger) {
            return new GoblinRanger();
        } else if (monster instanceof Hobgoblin) {
            return new Hobgoblin();
        } else if (monster instanceof Troll) {
            return new Troll();
        } else {
            return new Wolf();
        }
    }

    public Creature[] moveToDirection(MovementDirections movementDirections) {
        try {
            moveCreatureTo(creaturesList.get("player"), currentCel.getNeighbour(movementDirections));
        } catch (ExceptionHandler exceptionHandler) {
            if (exceptionHandler.getMessage().contains("monsterXX")) {
                Creature[] creatureslist = new Creature[currentCel.getCreatures().size()];
                return currentCel.getCreatures().toArray(creatureslist);
            } else {
                System.out.println(exceptionHandler.getMessage());
            }
        }
        Creature[] creatureslist = new Creature[currentCel.getCreatures().size()];
        return currentCel.getCreatures().toArray(creatureslist);
    }

    private void moveCreatureTo(Creature creature, MapCel cel) throws ExceptionHandler {
        if (currentCel.hasMonsters()) {
            throw new ExceptionHandler("clear the monsters from your pad");
        } else if (disabledCels.contains(cel.toString()) || cel.toString().equals("black ocean")) {
            throw new ExceptionHandler("you can not go this way");
        } else if (cel.getCreatures().contains(creature)) {
            currentCel = cel;
        } else {
            for (MapCelItems celElement : currentCel.getCelElements()) {
                if (celElement.isObstacle() && !celElement.isDeobstacle()) {
                    throw new ExceptionHandler("unlock this " + celElement.getDescription());
                }
            }
            currentCel = cel;
            for (Creature creatureInList : creaturesList.values()) {
                if (creatureInList instanceof Player) {
                    currentCel.addCreatureToCel(creatureInList);
                }
            }
        }
    }

    public void celMonstersKilled() {
        currentCel.getCreatures().removeIf(creature -> creature instanceof Monster);
    }

    public void collectCelItem(String itemName) {
        int i = currentCel.getCelElements().indexOf(new MapCelItems(itemName.strip().toLowerCase(), false, false, true));
        if (i >= 0) {
            collectedItemList.add(currentCel.getCelElements().get(i));
            System.out.println(itemName + " collected");
        }
        System.out.println("no such item");
    }

    public void addDisabledCel(String celType) {
        if (disabledCels == null) {
            disabledCels = new HashSet<>();
        }
        disabledCels.add(celType);
    }


    public void describeSurrounding() throws ExceptionHandler {
        if (currentCel.hasMonsters()) {
            throw new ExceptionHandler("monsterXX");
        }
        if (!currentCel.getCelElements().isEmpty()) {
            System.out.println(".".repeat(20));
            System.out.println("on your pad you see ");
            for (MapCelItems celElement : currentCel.getCelElements()) {
                System.out.printf("-");
                if (celElement.isObstacle() && celElement.isDeobstacle()) {
                    System.out.println("unlocked " + celElement.getDescription());
                } else if (celElement.isObstacle() && !celElement.isDeobstacle()) {
                    System.out.println(celElement.getDescription());
                } else if (celElement.isDeobstacle()) {
                    System.out.println(celElement.getDescription() + " key");
                } else {
                    System.out.println(celElement.getDescription());
                }
            }
        }
        System.out.println();
        for (MovementDirections value : MovementDirections.values()) {
            System.out.println("to the " + value.name().toLowerCase() + " you see " +(currentCel.getNeighbour(value) == null? "black ocean" : currentCel.getNeighbour(value)));
        }
        System.out.println("*".repeat(20));
    }


}
