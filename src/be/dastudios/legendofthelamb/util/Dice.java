package be.dastudios.legendofthelamb.util;

import be.dastudios.legendofthelamb.characters.professions.Profession;

import java.util.Random;

public class Dice {
    public static int throwDice(Profession profession){
        Random rand = new Random();
        int lifeDice = rand.nextInt(profession.getMaxLifeDice())+1;
        return lifeDice;
    }

    public static int throwDice(int maxDice){
        Random rand = new Random();
        int lifeDice = rand.nextInt(maxDice)+1;
        return lifeDice;
    }
}
