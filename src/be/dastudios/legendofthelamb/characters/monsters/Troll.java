package be.dastudios.legendofthelamb.characters.monsters;

import be.dastudios.legendofthelamb.util.Dice;

public class Troll implements Monster{
    private int AC = 19;
    private int maxHP = 40;
    private int currentHP = 40;
    private CR cr = CR.CR4;
    private int attack;
    private int damage;
    private String name = "Troll";

    @Override
    public String getName() {
        return name;
    }

    public int getAC() {
        return AC;
    }

    public int getMaxHP() {
        return maxHP;
    }

    public int getCurrentHP() {
        return currentHP;
    }

    public void setCurrentHP(int currentHP) {
        if(currentHP>maxHP){
            this.currentHP = maxHP;
        }else{
            this.currentHP = currentHP;
        }

    }

    public int getCR() {
        return 4;
    }
    public int  getXPfromCR() {
        return cr.getXp();
    }

    public int getAttack() {
        return Dice.throwDice(20) + 5;
    }

    public int getDamage() {
        return Dice.throwDice(10) + 4;
    }

    @Override
    public String toString(){
        return String.format("Troll{ AC = %d, maxHP = %d, currentHP = %d, CR = %d }", AC, maxHP, currentHP, getCR());
    }
}
