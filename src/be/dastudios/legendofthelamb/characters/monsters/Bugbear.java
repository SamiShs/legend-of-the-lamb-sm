package be.dastudios.legendofthelamb.characters.monsters;

import be.dastudios.legendofthelamb.characters.Creature;
import be.dastudios.legendofthelamb.characters.Player;
import be.dastudios.legendofthelamb.util.Dice;

public class Bugbear implements Monster{
    private int AC = 18;
    private int maxHP = 34;
    private int currentHP = 34;
    private CR cr = CR.CR3;
    private int attack;
    private int damage;
    private int attacksAmount = 2;
    private String name = "Bugbear";

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getAttacksAmount() {
        return attacksAmount;
    }

    public int getAC() {
        return AC;
    }

    public int getMaxHP() {
        return maxHP;
    }

    public int getCurrentHP() {
        return currentHP;
    }

    public void setCurrentHP(int currentHP) {
        if(currentHP>maxHP){
            this.currentHP = maxHP;
        }else{
            this.currentHP = currentHP;
        }

    }

    public int getCR(){
        return 3;
    }

    public int  getXPfromCR() {
        return cr.getXp();
    }

    public int getAttack() {
        return Dice.throwDice(20) + 5;
    }

    public int getDamage() {
        return Dice.throwDice(10) + 4;
    }

    @Override
    public String toString(){
        return String.format("Bugbear{ AC = %d, maxHP = %d, currentHP = %d, CR = %d }", AC, maxHP, currentHP, getCR());
    }

}
