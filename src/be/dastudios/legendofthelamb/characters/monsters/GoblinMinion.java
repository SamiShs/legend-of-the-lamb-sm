package be.dastudios.legendofthelamb.characters.monsters;

import be.dastudios.legendofthelamb.characters.Player;
import be.dastudios.legendofthelamb.characters.items.Weapon;
import be.dastudios.legendofthelamb.util.Dice;

public class GoblinMinion implements Monster{
    private int AC = 13;
    private int maxHP = 1;
    private int currentHP = 1;
    private CR cr = CR.CR0;
    private int attack;
    private int damage;
    private String name = "Goblin Minion";

    @Override
    public String getName() {
        return name;
    }

    public int getAC() {
        return AC;
    }

    public int getMaxHP() {
        return maxHP;
    }

    public int getCurrentHP() {
        return currentHP;
    }

    public void setCurrentHP(int currentHP) {
        if(currentHP>maxHP){
            this.currentHP = maxHP;
        }else{
            this.currentHP = currentHP;
        }

    }

    public int getCR() {
        return 0;
    }
    public int  getXPfromCR() {
        return cr.getXp();
    }

    public int getAttack() {
        return Dice.throwDice(20) + 3;
    }

    public int getDamage() {
        return Dice.throwDice(4) + 2;
    }

    @Override
    public String toString(){
        return String.format("GoblinMinion{ AC = %d, maxHP = %d, currentHP = %d, CR = %d }", AC, maxHP, currentHP, getCR());
    }

}
