package be.dastudios.legendofthelamb.util;

import be.dastudios.legendofthelamb.area.MainMap;
import be.dastudios.legendofthelamb.area.MapCel;
import be.dastudios.legendofthelamb.area.MovementDirections;
import be.dastudios.legendofthelamb.characters.Creature;
import be.dastudios.legendofthelamb.characters.Player;
import be.dastudios.legendofthelamb.characters.monsters.Monster;
import be.dastudios.legendofthelamb.characters.professions.Fighter;
import be.dastudios.legendofthelamb.characters.professions.Healer;
import be.dastudios.legendofthelamb.characters.professions.Profession;
import be.dastudios.legendofthelamb.characters.professions.Ranger;
import be.dastudios.legendofthelamb.characters.races.Dwarf;
import be.dastudios.legendofthelamb.characters.races.Elf;
import be.dastudios.legendofthelamb.characters.races.Human;
import be.dastudios.legendofthelamb.characters.races.Race;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.NavigableMap;
import java.util.stream.Stream;

public class CommandHandler {
    public static boolean handelerALive = true;
    private static Player[] player;
    private static MainMap mainMap;
    private static SavingHandler savingHandler;

    public static void handleNavigation() {
        handleNavigation("");
    }

    public static void handleNavigation(String promptMessage) {
        if (promptMessage.isEmpty()) {
            promptMessage = "Choose direction";
        }
        Creature[] celCreatures = null;
        String input = Keyboard.readString(promptMessage);
        switch (input.strip().toLowerCase()) {
            case "go north":
                celCreatures = mainMap.moveToDirection(MovementDirections.NORTH);
                break;
            case "go south":
                celCreatures = mainMap.moveToDirection(MovementDirections.SOUTH);
                break;
            case "go east":
                celCreatures = mainMap.moveToDirection(MovementDirections.EAST);
                break;
            case "go west":
                celCreatures = mainMap.moveToDirection(MovementDirections.WEST);
                break;
            case "save":
                savingMenu();
                break;
            default:
                menuNavigation(input);
                break;
        }
        if (handelerALive) {
            try {
                mainMap.describeSurrounding();
            } catch (ExceptionHandler exceptionHandler) {
                if (exceptionHandler.getMessage().contains("monsterXX")) {
                    System.out.println("!!!!Monsters!!!!");
                    handleFight("choose your action", celCreatures);
                }
            }
            handleNavigation();
        }
    }

    private static void savingMenu() {
        if (savingHandler == null) {
            String input = Keyboard.readString("give a name for your saving");
            savingHandler = new SavingHandler(player, mainMap, input);
            FilesHandeler.save(savingHandler);
        } else {
            if (Keyboard.askYesNo("Do you want to save to " + savingHandler.getSaveFileName())) {
                savingHandler = new SavingHandler(player, mainMap, savingHandler.getSaveFileName());
                FilesHandeler.save(savingHandler);
            } else {
                savingHandler = null;
                savingMenu();
            }
        }
        handelerALive = !Keyboard.askYesNo("you want to close");
    }


    public static void handleFight(String promptMessage, Creature... creatures) {
        List<Creature> list = new ArrayList<>(Arrays.asList(creatures));
        list.sort((l, l1) -> Math.max(l.getInitiative(), l1.getInitiative()));
        creatures = list.toArray(Creature[]::new);
        boolean monstersAlive = true;
        boolean playersAlive = true;
        while (playersAlive && monstersAlive) {
            for (Creature creature : creatures) {
                if (creature.getCurrentHP() > 0) {
                    System.out.printf("--------------------\nTurn of %s\n--------------------\n", creature.toString());
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException ie) {
                        ie.printStackTrace();
                    }
                    for (int i = 0; i < creature.getAttacksAmount(); i++) {
                        boolean correctMove = false;
                        Creature targetCreature;

                        if (creature instanceof Player) {
                            targetCreature = creatures[Keyboard.chooseOption(creatures, "choose the target")];
                            while (correctMove == false) {
                                String input = Keyboard.readString(promptMessage);
                                switch (input) {
                                    case "use power 1":
                                        ((Player) creature).usePower1(targetCreature);
                                        correctMove = true;
                                        break;
                                    case "use power 3":
                                        ((Player) creature).usePower3(targetCreature);
                                        correctMove = true;
                                        break;
                                    case "use power 4":
                                        ((Player) creature).usePower4(targetCreature);
                                        correctMove = true;
                                        break;
                                    case "use power 5":
                                        if (((Player) creature).getProfession() instanceof Fighter) {
                                            ((Player) creature).usePower5(targetCreature);
                                            correctMove = true;
                                        } else {
                                            ((Player) creature).usePower5(creatures);
                                            correctMove = true;
                                        }
                                        break;
                                    default:
                                        System.out.println("Uncorrect command: try again");
                                        break;
                                }

                            }
                        } else if (creature instanceof Monster) {
                            for (Creature target : creatures) {
                                if (target instanceof Player) {
                                    ((Monster) creature).level1Power((Player) target);
                                    break;
                                }
                            }

                        }

                    }
                }
            }
            int monsters = 0;
            for (Creature monster : creatures) {
                if (monster instanceof Monster) {
                    monsters++;
                }
            }
            int players = creatures.length - monsters;
            int monstersDead = 0;
            for (int i = 0; i < creatures.length; i++) {
                if (creatures[i] instanceof Monster && creatures[i].getCurrentHP() <= 0) {
                    monstersDead++;
                }
            }
            if (monstersDead == monsters) {
                monstersAlive = false;
            }
            int playersDead = 0;
            for (int i = 0; i < creatures.length; i++) {
                if (creatures[i] instanceof Player && creatures[i].getCurrentHP() <= 0) {
                    playersDead++;
                }
            }
            if (player == null) {

            }
            if (playersDead == players) {
                playersAlive = false;
            }
        }
        if (playersAlive) {
            int currentLevel;
            int gainedXP = Stream.of(creatures).filter((c) -> c instanceof Monster).mapToInt(c -> ((Monster) c).getXPfromCR()).sum();
            System.out.println("--------------------\nYOU WON!\n--------------------");
            for (Creature player : creatures) {
                if (player instanceof Player) {
                    currentLevel = ((Player) player).getLevel();
                    ((Player) player).gainXP(gainedXP);
                    for (int i = 0; i < ((Player) player).getLevel() - currentLevel; i++) {
                        ((Player) player).levelUpMaxHP();
                        player.setCurrentHP(player.getMaxHP());
                        System.out.printf("%s went from level %d to level %d!\nYour maxHP increased to %d!\n", player.getName(), currentLevel + i, currentLevel + i + 1, player.getMaxHP());
                    }
                }
            }
            //Stream.of(creatures).filter((c) -> c instanceof Player).forEach((c) -> ((Player) c).gainXP(gainedXP));

            Stream.of(creatures).filter((c) -> c instanceof Player).forEach((c) -> System.out.println(((Player) c).getXp()));
            mainMap.celMonstersKilled();
            handleNavigation("");
        } else {
            System.out.println("--------------------\nYOU DIED!\n--------------------");
            loadlastSave();
        }
    }

    private static void loadlastSave() {
        if (savingHandler == null) {
            System.out.println("no last checkpoint");
            player = null;
        } else {
            savingHandler = FilesHandeler.loadSave(savingHandler,savingHandler.getSaveFileName());
        }

    }

    public static void menuNavigation(String input) {
        switch (input.strip().toLowerCase()) {
            case "new":
                int inputPlayerCount = Keyboard.askForNumber("how many players are playing", 3);
                player = new Player[inputPlayerCount];
                for (int i = 0; i < inputPlayerCount; i++) {
                    createNewPlayer(i);
                }
                createMap();
                mainMap.startJourneyFor(player[0], 20,3);
                for (int i = 1; i < player.length; i++) {
                    mainMap.addPlayer(player[i]);
                }
                handleNavigation();
                break;
            case "load":
                loadMenu();
                mainMap.describeSurrounding();
                handleNavigation();
                break;
            case "reset":
                break;
            case "controls":
                break;
            case "settings":
                break;
            case "quit":
                handelerALive = false;
                break;
            default:
                System.out.println("wrong command");
                break;
        }
    }

    private static void loadMenu() {
        savingHandler = FilesHandeler.loadSave(null);
        player = savingHandler.getPlayer();
        mainMap = savingHandler.getMainMap();
    }

    private static void createMap() {
        mainMap = new MainMap();
    }

    private static void createNewPlayer(int next) {
        String name = Keyboard.readString(""+(next==0?"MainPlayer":"player"+(next+1))+ " name");
        Profession profession = null;
        int input = Keyboard.chooseOption(new String[]{"figher", "healer", "ranger"}, "choose your profession");
        switch (input) {
            case 0:
                profession = new Fighter();
                break;
            case 1:
                profession = new Healer();
                break;
            case 2:
                profession = new Ranger();
                break;
        }
        Race race = null;
        input = Keyboard.chooseOption(new String[]{"Dwarf", "Elf", "Human"}, "choose your profession");
        switch (input) {
            case 0:
                race = new Dwarf();
                break;
            case 1:
                race = new Elf();
                break;
            case 2:
                race = new Human();
                break;
        }
        player[next] = new Player(profession, race, name);
    }

}
