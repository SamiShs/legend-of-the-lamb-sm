package be.dastudios.legendofthelamb.characters.items;

import be.dastudios.legendofthelamb.characters.Player;

public enum Armour {
    CHAIN_MAIL{
        @Override
        public int getArmourBase(Player player) {
            return 10 + (player.getAbilities().get("Strength")-10)/2 + player.getProfession().getArmourBase();
        }
    },
    LEATHER_ARMOUR{
        @Override
        public int getArmourBase(Player player) {
            return 8 + (player.getAbilities().get("Dexterity")>player.getAbilities().get("Strength")?(player.getAbilities().get("Dexterity")-10)/2:(player.getAbilities().get("Strength")-10)/2) + player.getProfession().getArmourBase();
        }
    },
    CLOTHES{
        @Override
        public int getArmourBase(Player player) {
            return 4 + (player.getAbilities().get("Dexterity")-10)/2 + player.getProfession().getArmourBase();
        }
    };

    public int getArmourBase(Player player) {
        return 0;
    }
}
