package be.dastudios.legendofthelamb.characters.races;

import java.util.HashMap;
import java.util.Map;

public class Human implements Race {
    private Map<String, Integer> abilities = new HashMap<>();
    private int baseSpeed = 6;

    public Human() {
        setMap();
    }

    private void setMap(){
        abilities.put("Strength", 1);
        abilities.put("Constitution", 1);
        abilities.put("Dexterity", 1);
        abilities.put("Wisdom", 1);
        abilities.put("Intelligence", 1);
        abilities.put("Charisma", 1);
    }

    public Map<String, Integer> getAbilities(){
        return abilities;
    }

    public int getBaseSpeed() {
        return baseSpeed;
    }
}
