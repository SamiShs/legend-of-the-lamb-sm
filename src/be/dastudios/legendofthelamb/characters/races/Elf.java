package be.dastudios.legendofthelamb.characters.races;

import java.util.HashMap;
import java.util.Map;

public class Elf implements Race {
    private Map<String, Integer> abilities = new HashMap<>();
    private int baseSpeed = 7;

    public Elf() {
        setMap();
    }

    private void setMap(){
        abilities.put("Strength", 0);
        abilities.put("Constitution", 0);
        abilities.put("Dexterity", 2);
        abilities.put("Wisdom", 0);
        abilities.put("Intelligence", 2);
        abilities.put("Charisma", 0);
    }

    public Map<String, Integer> getAbilities(){
        return abilities;
    }

    public int getBaseSpeed() {
        return baseSpeed;
    }
}
