package be.dastudios.legendofthelamb.characters.professions;

import be.dastudios.legendofthelamb.characters.Creature;
import be.dastudios.legendofthelamb.characters.Player;
import be.dastudios.legendofthelamb.characters.items.Armour;
import be.dastudios.legendofthelamb.characters.items.Weapon;

import java.io.Serializable;
import java.util.Map;

public interface Profession extends Serializable {
    Map<String, Integer> getAbilities();
    int getMaxLifeDice();
    int getArmourBase();
    void level1Power(Player player, Creature creature);
    void level3Power(Player player, Creature creature);
    void level4Power(Player player, Creature creature);
    void level5Power(Player player, Creature creature);
    Weapon getWeapon();
    void setWeapon(Weapon weapon);
    Armour getArmour();
    void setArmour(Armour armour);
    default int getAttacksAmount(Player player){
        return 1;
    }
}
