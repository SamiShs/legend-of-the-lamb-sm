package be.dastudios.legendofthelamb;

import be.dastudios.legendofthelamb.util.CommandHandler;
import be.dastudios.legendofthelamb.util.Keyboard;

public class Menu {
    private String description="Menu";

    public static void bootUp() {
        System.out.println("+++++++++++++++++++++++++++++\n" +
                "++ Legend of the Lamb – DA ++\n" +
                "+++++++++++++++++++++++++++++\n" +
                "-- DA Studios Belgium --\n" +
                "-----------------------------\n" +
                "Loading Maps and Save Games\n" +
                "++++++++++\n" +
                "++ DONE ++\n" +
                "++++++++++");
        while (CommandHandler.handelerALive) {
            String[] mainOptions = new String[]{"New – Start a New Game", "Load – Load a Saved Game", "Reset – Reset all Saved Files",
                    "Controls – Game Controls", "Settings – Game Settings", "Quit - Quit Game"};
            String input = Keyboard.writeOption(mainOptions, "What do you want to do?");
            CommandHandler.menuNavigation(input);
        }


    }
}
