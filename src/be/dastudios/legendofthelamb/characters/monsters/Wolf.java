package be.dastudios.legendofthelamb.characters.monsters;

import be.dastudios.legendofthelamb.characters.Player;
import be.dastudios.legendofthelamb.util.Dice;

public class Wolf implements Monster{
    private int AC = 19;
    private int maxHP = 40;
    private int currentHP = 40;
    private CR cr = CR.CR_HALF;
    private int attack;
    private int damage;
    private String name = "Wolf";

    @Override
    public String getName() {
        return name;
    }

    public int getAC() {
        return AC;
    }

    public int getMaxHP() {
        return maxHP;
    }

    public int getCurrentHP() {
        return currentHP;
    }

    public void setCurrentHP(int currentHP) {
        if(currentHP>maxHP){
            this.currentHP = maxHP;
        }else{
            this.currentHP = currentHP;
        }

    }

    public int getCR() {
        return 1/2;
    }
    public int  getXPfromCR() {
        return cr.getXp();
    }

    public int getAttack() {
        return Dice.throwDice(20) + 3;
    }

    public int getDamage() {
        return Dice.throwDice(6) + 3;
    }

    @Override
    public String toString(){
        return String.format("Wolf{ AC = %d, maxHP = %d, currentHP = %d, CR = %d }", AC, maxHP, currentHP, getCR());
    }

}
