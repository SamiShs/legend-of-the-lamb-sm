package be.dastudios.legendofthelamb.characters.professions;

import be.dastudios.legendofthelamb.characters.Creature;
import be.dastudios.legendofthelamb.characters.Player;
import be.dastudios.legendofthelamb.characters.items.Armour;
import be.dastudios.legendofthelamb.characters.items.Weapon;
import be.dastudios.legendofthelamb.characters.monsters.Monster;
import be.dastudios.legendofthelamb.util.Dice;

import java.util.HashMap;
import java.util.Map;

public class Fighter implements Profession {
    private Map<String, Integer> abilities = new HashMap<>();
    private int armourBase = 8;
    private int maxLifeDice = 12;
    private Weapon weapon;
    private Armour armor;
    private int possibleAttacksAmount = 1;



    public int getPossibleAttacksAmount() {
        return possibleAttacksAmount;
    }

    public void setPossibleAttacksAmount(int possibleAttacksAmount) {
        this.possibleAttacksAmount = possibleAttacksAmount;
    }

    public Fighter(){
        setMap();
        setWeapon(Weapon.SWORD);
        setArmour(Armour.CHAIN_MAIL);
    }

    private void setMap(){
        abilities.put("Strength", 18);
        abilities.put("Constitution", 15);
        abilities.put("Dexterity", 11);
        abilities.put("Wisdom", 14);
        abilities.put("Intelligence", 8);
        abilities.put("Charisma", 13);
    }

    public Map<String, Integer> getAbilities() {
        return abilities;
    }

    public int getMaxLifeDice() {
        return maxLifeDice;
    }

    public int getArmourBase(){
        return armourBase;
    }

    public void level1Power(Player player, Creature creature){
        if (creature == null) {
            System.out.println();
        } else {
            int tryAttack = weapon.getAttack(player);
            System.out.printf("%s attacks %s (AC = %d) for %d\n", player.getName(), creature.getName(), creature.getAC(), tryAttack);
            if(tryAttack>=creature.getAC()){
                System.out.println("Hit succesful!");
                int damage = weapon.getDamage(player);
                creature.setCurrentHP(creature.getCurrentHP()-damage);
                System.out.printf("%s wounds %s (maxHP = %d) for %d: currentHP = %d\n", player.getName(), creature.getName(), creature.getMaxHP(), damage, creature.getCurrentHP());
            }else{
                System.out.println("Hit failed!");
            }
        }
    }

    @Override
    public void level3Power(Player player, Creature creature) {
        System.out.println("You can attack 2 times");
    }

    @Override
    public void level4Power(Player player, Creature creature) {
        System.out.println("You can attack 3 times");
    }

    @Override
    public void level5Power(Player player, Creature creature) {
        System.out.println("If you hit the opponent, you instantly kill it.");
        int dice1 = Dice.throwDice(20);
        int dice2 = Dice.throwDice(20);
        int tryAttack = (dice1>dice2?dice2:dice1) + player.getLevel()/2 + (player.getAbilities().get("Strength")-10)/2;
        System.out.printf("%s attacks %s (AC = %d) for %d\n", player.getName(), creature.getName(), creature.getAC(), tryAttack);
        if(tryAttack>=creature.getAC()){
            System.out.println("Hit succesful!");
            int damage = creature.getCurrentHP();
            creature.setCurrentHP(creature.getCurrentHP()-damage);
            System.out.printf("%s KILLS %s (maxHP = %d) for %d: currentHP = %d\n", player.getName(), creature.getName(), creature.getMaxHP(), damage, creature.getCurrentHP());
        }else{
            System.out.println("Hit failed!");
        }
    }

    public void setWeapon(Weapon weapon){
        this.weapon = weapon;
    }

    @Override
    public Armour getArmour() {
        return armor;
    }

    @Override
    public void setArmour(Armour armour) {
        this.armor = armour;
    }

    @Override
    public int getAttacksAmount(Player player) {
        int attacks;
        if(player.getLevel()>=4){
            attacks=3;
        }else if(player.getLevel()>=3){
            attacks=2;
        }else{
            attacks=1;
        }
        return attacks;
    }

    @Override
    public Weapon getWeapon() {
        return weapon;
    }
}
