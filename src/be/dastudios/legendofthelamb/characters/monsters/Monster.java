package be.dastudios.legendofthelamb.characters.monsters;

import be.dastudios.legendofthelamb.characters.Creature;
import be.dastudios.legendofthelamb.characters.Player;

public interface Monster extends Creature {
    int getAC();
    int getMaxHP();
    int getCurrentHP();
    int getCR();
    int getAttack();
    int getDamage();
    int getXPfromCR();
    void setCurrentHP(int currentHP);
    default void level1Power(Player player){
        int tryAttack = getAttack();
        System.out.printf("%s attacks %s (AC = %d) for %d\n", getName(), player.getName(), player.getAC(), tryAttack);
        if(tryAttack>=player.getAC()){
            System.out.println("Hit succesful!");
            int damage =getDamage();
            player.setCurrentHP(player.getCurrentHP()-damage);
            System.out.printf("%s %s %s (maxHP = %d) for %d: currentHP = %d\n", getName(), player.getCurrentHP()<=0?"KILLS":"wounds", player.getName(), player.getMaxHP(), damage, player.getCurrentHP());
        }else{
            System.out.println("Hit failed!");
        }
    }
    default int getAttacksAmount(){
        return 1;
    }
}
