package be.dastudios.legendofthelamb.characters;

import be.dastudios.legendofthelamb.characters.monsters.Monster;
import be.dastudios.legendofthelamb.characters.professions.Healer;
import be.dastudios.legendofthelamb.characters.professions.Profession;
import be.dastudios.legendofthelamb.characters.professions.Ranger;
import be.dastudios.legendofthelamb.characters.races.Race;
import be.dastudios.legendofthelamb.util.Dice;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Player implements Creature, Serializable {
    private Profession profession;
    private Race race;
    private int level;
    private int xp;
    private int maxHP;
    private int currentHP;
    private String name;
    private Map<String, Integer> abilities = new HashMap<>();

    public int getAttacksAmount() {
        return profession.getAttacksAmount(this);
    }

    public int getAC() {
        return profession.getArmour().getArmourBase(this);
    }



    public int getCurrentHP() {
        return currentHP;
    }

    public int getMaxHP() {
        return maxHP;
    }

    public void setCurrentHP(int currentHP) {
        if(currentHP>maxHP){
            this.currentHP = maxHP;
        }else{
            this.currentHP = currentHP;
        }

    }

    public Player(Profession profession, Race race, String name){
        this(profession, race, 1, 0, name);
    }

    public Player(Profession profession, Race race, int level, int xp, String name) {
        this.profession = profession;
        this.race = race;
        this.level = level;
        this.xp = xp;
        this.name = name;
        setMaxHP();
        setAbilities();
        setCurrentHP(maxHP);
    }

    private void setMaxHP(){
        maxHP = Dice.throwDice(profession) + Dice.throwDice(profession) + Dice.throwDice(profession) + 10;
    }

    public void levelUpMaxHP(){
        maxHP += Dice.throwDice(profession)*2;
    }

    private void setAbilities(){
        profession.getAbilities().forEach( (key,value) -> race.getAbilities().merge( key, value, (v1, v2) -> v1 + v2));
        abilities = race.getAbilities();
    }

    public void usePower1(Creature creature){
        profession.level1Power(this, creature);
    }

    public void usePower3(Creature creature){
        if(getLevel()>=3){
            profession.level3Power(this, creature);
        }else{
            System.out.println("You need to be level 3 or higher to use this Power!");
        }

    }

    public void usePower4(Creature creature){
        if(getLevel()>=4){
            profession.level4Power(this, creature);
        }else{
            System.out.println("You need to be level 4 or higher to use this Power!");
        }
    }

    public void usePower5(Creature creature){
        if(getLevel()>=5){
            if(profession instanceof Healer){
                int dice = Dice.throwDice(6);
                System.out.println(dice);
                if(dice==5 || dice==6){
                    System.out.println("Gods area was succesful!");
                    profession.level5Power(this, creature);
                }else{
                    System.out.println("Gods area failed!");
                }
            }else{
                if(profession instanceof Ranger){
                    System.out.println("You shoot an arrow in the air that explodes and rains down arrows on all enemies.");
                }
                profession.level5Power(this, creature);
            }
        }else{

            System.out.println("You need to be level 5 or higher to use this Power!");
        }
    }

    public void usePower5(Creature... creatures){
        if(getLevel()>=5){
            if(profession instanceof Healer){
                int dice = Dice.throwDice(6);
                System.out.println(dice);
                if(dice==5 || dice==6){
                    System.out.println("Gods area was succesful!");
                    for (Creature creature:creatures) {
                        profession.level5Power(this, creature);
                    }
                }else{
                    System.out.println("Gods area failed!");
                }
            }else{
                if(profession instanceof Ranger){
                    System.out.println("You shoot an arrow in the air that explodes and rains down arrows on all enemies.");
                }
                for (Creature creature:creatures) {
                    profession.level5Power(this, creature);
                }
            }
        }else{
            System.out.println("You need to be level 5 or higher to use this Power!");
        }
    }

    public Profession getProfession() {
        return profession;
    }

    public Race getRace() {
        return race;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getLevel() {
        if(xp>=0 && xp<100){
            level = 1;
        }else if(xp>=100 && xp<250){
            level = 2;
        }else if(xp>= 250 && xp<700){
            level = 3;
        }else if(xp>= 700 && xp<1500){
            level = 4;
        }else{
            level = 5;
        }

        return level;
    }

    public int getXp() {
        return xp;
    }



    public String getName() {
        return name;
    }

    public Map<String, Integer> getAbilities() {
        return abilities;
    }

    public void gainXP(int extraXP){
        xp += extraXP;
    }

    @Override
    public int getInitiative(){
        return Dice.throwDice(20) + (getAbilities().get("Dexterity")-10)/2;
    }

    @Override
    public String toString(){
        return String.format("%s{ AC = %d, maxHP = %d, currentHP = %d, level = %d }", getName(), getAC(), getMaxHP(), getCurrentHP(), getLevel());
    }

}
