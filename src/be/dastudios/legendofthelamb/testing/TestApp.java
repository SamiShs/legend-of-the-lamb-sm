package be.dastudios.legendofthelamb.testing;

import be.dastudios.legendofthelamb.area.*;
import be.dastudios.legendofthelamb.characters.Creature;
import be.dastudios.legendofthelamb.characters.Player;
import be.dastudios.legendofthelamb.characters.professions.Fighter;
import be.dastudios.legendofthelamb.characters.races.Human;
import be.dastudios.legendofthelamb.util.FilesHandeler;
import be.dastudios.legendofthelamb.util.Keyboard;

import java.util.*;

public class TestApp {
    public static void main(String[] args) {
        String defaultMap = ";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;\n" +
                ";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;\n" +
                ";;VVVVVVVVVVVVVVVVXXXXXXXXXXXXXXXXX;;;;;\n" +
                ";;V;;;;;;;;;;;;;;;X;;;;;;;;;;;;;;;X;;;;;\n" +
                ";;V;;;;;;;;;;;;;;;X;;;;;;;;;;;;;;;X;;;;;\n" +
                ";;V;;;;;;;;;;;;;;;X;;;;;;;;;;;;;;;X;;;;;\n" +
                ";;V;;;;;;;;;;;;;;;X;;;;;;;;;;;;;;;X;;;;;\n" +
                ";;V;;;;;;;;;;;;;;;X;;;;;;;;;;;;;;;X;;;;;\n" +
                ";;V;;;;;;;;;;;;;;;X;;;;;;;;;;;;;;;X;;;;;\n" +
                ";;V;;;;;;;;;;;;;;;X;;;;;;;;;;;;;;;X;;;;;\n" +
                ";;V;;;;;;;;;;;;;;;X;;;;;;;;;;;;;;;X;;;;;\n" +
                ";;K;;;;;;;;;;;;;;;X;;;;;;;;;;;;;;;X;;;;;\n" +
                ";;VVVVVVVVVVVVVVVVX;;;;;;;;;;;;;;;X;;;;;\n" +
                ";;K;;;;;;;;;;;;;;;XXXXXXXXD;;;;;;;X;;;;;\n" +
                ";;V;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;X;;;;;\n" +
                ";;V;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;X;;;;;\n" +
                ";;V;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;X;;;;;\n" +
                ";;V;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;X;;;;;\n" +
                ";;V;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;X;;;;;\n" +
                ";;V;;;;;;;;;;;;;VVVVVVVVVVVVW;;;;;X;;;;;\n" +
                ";;V;;;;;;;;;;;;;V;;;;;;;;;;;;;;;;;X;;;;;\n" +
                ";XXXXXXXXXXX;;;;V;;;;;;;;;;;;;;;;;X;;;;;\n" +
                ";X;;;;;;;;;X;;;;V;;;;;;;;;;;;;;;;;X;;;;;\n" +
                ";X;;;;;;;;;XVVVVV;;;;;;;;;;;;;;;;;X;;;;;\n" +
                ";X;;;;;;;;;X;;;;;;;;;;;;;;;;;;;;;;X;;;;;\n" +
                ";X;;;;;;;;;X;;;;;;XXXXX;;;;;;;;;;;X;;;;;\n" +
                ";X;;;;;;;;;X;;;;;;X;;;X;;;;;;;;;;;X;;;;;\n" +
                ";X;;;;;;;;;X;;;;;;X;;;X;;;;;;;;;;;X;;;;;\n" +
                ";X;;;;;;;;;X;;;;;;X;;;X;;;;;;;;;;;X;;;;;\n" +
                ";X;;;;;;;;;XXXXXXXX;;;X;;;;;;;;;;;X;;;;;\n" +
                ";X;;;;;;;;;;;;;;;;;;;;X;;;;;XXXXXXX;;;;;\n" +
                ";X;;;;;;;;;;;;;;;;;;;;X;;;;;X;;;V;;;;;;;\n" +
                ";X;;;;;;;;;;;;;;;;;;;;X;;;;;X;;;VVVVVVV;\n" +
                ";X;;;;;;;;;;;;;;;;;;;;X;;;;;X;;;V;;;;;V;\n" +
                ";XXXXXXXX;;;;;;;;XXXXXX;;;;;X;;;V;;;;;V;\n" +
                ";;;;;;;;X;;;;;;;;X;;;;;;;;;;X;VVV;;;;;V;\n" +
                ";;;;;;;;X;;;;;;;;XXXXXXXXXXXX;V;;;;;;;V;\n" +
                ";;;;;;XXX;;;;;;;;;;;;;;;;;;;;;VVVVVVVVV;\n" +
                ";;;;;;X;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;\n" +
                ";;;;;;S;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;";

        char[][] mapArea = new char[40][40];
        MapCel[][] areaObjects = new MapCel[40][40];
        String[] mapRow = defaultMap.split("\n");
        for (int i = 0; i < 40; i++) {
            mapArea[i] = mapRow[i].toCharArray();
        }
        Arrays.stream(mapArea).forEach(strings -> System.out.println(Arrays.toString(strings)));
        int[] lastCord = new int[2];
        for (int i = 0; i < 40; i++) {
            for (int j = 0; j < 40; j++) {
                if (mapArea[i][j] != ';' && mapArea[i][j] != 0) {
                    MapCel cel = new MapCel("pad", new int[]{i, j});
                    switch (mapArea[i][j]) {
                        case 'S':
                            cel.addCelElements(new MapCelItems("start flag"));
                            break;
                        case 'D':
                            cel.addCelElements(new MapCelItems("end flag"));
                            break;
                        case 'X':
                            break;
                        case 'V':
                            cel.setDescription("small pad");
                            break;
                        case 'K':
                            cel.addCelElements(new MapCelItems("door", true, false, false));
                            break;
                        case 'W':
                            cel.addCelElements(new MapCelItems("Bugbear leader", true));
                            cel.addCelElements(new MapCelItems("door", false, true,true));
                            break;
                    }
                    for (MovementDirections value : MovementDirections.values()) {
                        int[] k = value.calculateDirectionCoordinate(new int[]{i, j});
                        if (k[0] > -1 && k[1] > -1 && k[0] < 40 && k[1] < 40) {
                            if (mapArea[k[0]][k[1]] == ';' && areaObjects[k[0]][k[1]] == null) {
                                areaObjects[k[0]][k[1]] = new MapCel(BasicLandscapeCel.values()[new Random().nextInt(4)].description(), k);
                            }
                        }
                        areaObjects[i][j] = cel;
                    }

                }
            }
        }
        for (MapCel[] areaObject : areaObjects) {
            for (MapCel mapCel : areaObject) {
                if (mapCel != null && (mapCel.toString().equals("small pad") || mapCel.toString().equals("pad"))) {
                    for (MovementDirections value : MovementDirections.values()) {
                        System.out.println(Arrays.toString(mapCel.getCelCoordinates()));
                            int[] coord = value.calculateDirectionCoordinate(mapCel.getCelCoordinates());
                        System.out.println(Arrays.toString(coord));

                            if (coord[0] > -1 && coord[1] > -1 && coord[0] < 40 && coord[1] < 40) {
                                mapCel.addCelNeighbour(value,areaObjects[coord[0]][coord[1]]);
                            }

                    }
                }

            }
        }

        Arrays.stream(areaObjects).forEach(mapObjects -> {Arrays.stream(mapObjects).map(String::valueOf).forEach(s -> System.out.print((s.replace("null", "") + "-----").substring(0, 3)+","));
            System.out.println("");});
        FilesHandeler.writeFile(areaObjects);
//
//        MainMap mainMap = new MainMap();
//        Player player = new Player(new Fighter(),new Human(),"Samir");
//        mainMap.startJourneyFor(player, 20,2);
//        for (int i = 0; i < 20; i++) {
//            int input = Keyboard.chooseOption(MovementDirections.values(), "choose movement");
//            Creature[] creatues = mainMap.moveToDirection(MovementDirections.values()[input], player.getName());
//            System.out.println(Arrays.toString(creatues));
//            if (creatues.length > 1) {
//                mainMap.celMonstersKilled();
//            }
//        }

//////                Arrays.stream(ma).forEach(s -> )));


//        MapCel[][] areaObjects = new MapCel[41][41];
//        areaObjects = FilesHandeler.loadFile(areaObjects, ".map");
//        Arrays.stream(areaObjects).forEach(mapObjects -> {Arrays.stream(mapObjects).map(String::valueOf).forEach(s -> System.out.print((s.replace("null", "") + "-----").substring(0, 3)+","));
//           System.out.println("");});

//
//        MapGenerator.generateLandscape(areaObject, objectList);
//        Arrays.stream(areaObject).forEach(mapObjects -> System.out.println(Arrays.toString(mapObjects).replaceAll("null"," ")));
    }
}
