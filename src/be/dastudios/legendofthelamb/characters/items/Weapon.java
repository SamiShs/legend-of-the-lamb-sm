package be.dastudios.legendofthelamb.characters.items;

import be.dastudios.legendofthelamb.characters.Player;
import be.dastudios.legendofthelamb.util.Dice;

public enum Weapon {
    SWORD{
        @Override
        public int getAttack(Player player) {
            return Dice.throwDice(20) + player.getLevel()/2 + (player.getAbilities().get("Strength")-10)/2;
        }
        @Override
        public int getDamage(Player player){
            return Dice.throwDice(10) + (player.getAbilities().get("Strength")-10)/2;
        }
    },
    BOW{
        @Override
        public int getAttack(Player player) {
            return Dice.throwDice(20) + player.getLevel()/2 + (player.getAbilities().get("Dexterity")-10)/2;
        }
        @Override
        public int getDamage(Player player){
            return Dice.throwDice(12) + (player.getAbilities().get("Dexterity")-10)/2;
        }
    },
    HOLY_SYMBOL{
        @Override
        public int getAttack(Player player) {
            return Dice.throwDice(20) + player.getLevel()/2 + (player.getAbilities().get("Wisdom")-10)/2;
        }
        @Override
        public int getDamage(Player player){
            return Dice.throwDice(6) + (player.getAbilities().get("Wisdom")-10)/2;
        }
    },
    DAGGER{
        @Override
        public int getAttack(Player player) {
            return Dice.throwDice(20) + player.getLevel()/2 + (player.getAbilities().get("Dexterity")>player.getAbilities().get("Strength")?(player.getAbilities().get("Dexterity")-10)/2:(player.getAbilities().get("Strength")-10)/2);
        }
        @Override
        public int getDamage(Player player){
            return Dice.throwDice(4) + (player.getAbilities().get("Dexterity")>player.getAbilities().get("Strength")?(player.getAbilities().get("Dexterity")-10)/2:(player.getAbilities().get("Strength")-10)/2);
        }
    };

    public int getAttack(Player player){
        return 0;
    }
    public int getDamage(Player player) {return 0;}
}
