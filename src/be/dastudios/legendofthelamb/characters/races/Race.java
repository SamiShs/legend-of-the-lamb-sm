package be.dastudios.legendofthelamb.characters.races;

import java.io.Serializable;
import java.util.Map;

public interface Race extends Serializable {
    int getBaseSpeed();
    Map<String, Integer> getAbilities();
}
