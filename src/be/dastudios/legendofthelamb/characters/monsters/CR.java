package be.dastudios.legendofthelamb.characters.monsters;

public enum CR {
    CR0(10),
    CR_QUARTER(25),
    CR_HALF(50),
    CR1(100),
    CR2(150),
    CR3(450),
    CR4(800),
    CR5(2000);

    private int xp;

    public int getXp() {
        return xp;
    }

    CR(int xp){
        this.xp = xp;
    }

}
