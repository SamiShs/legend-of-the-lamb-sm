package be.dastudios.legendofthelamb.characters.professions;

import be.dastudios.legendofthelamb.characters.Creature;
import be.dastudios.legendofthelamb.characters.Player;
import be.dastudios.legendofthelamb.characters.items.Armour;
import be.dastudios.legendofthelamb.characters.items.Weapon;
import be.dastudios.legendofthelamb.characters.monsters.Monster;
import be.dastudios.legendofthelamb.util.Dice;

import java.util.HashMap;
import java.util.Map;

public class Ranger implements Profession{
    private Map<String, Integer> abilities = new HashMap<>();
    private int maxLifeDice = 8;
    private int armourBase = 6;
    private Weapon weapon;
    private Armour armour;
    private int possibleAttacksAmount = 1;

    public Ranger(){
        setMap();
        setWeapon(Weapon.BOW);
        setArmour(Armour.LEATHER_ARMOUR);
    }

    private void setMap(){
        abilities.put("Strength", 8);
        abilities.put("Constitution", 15);
        abilities.put("Dexterity", 18);
        abilities.put("Wisdom", 13);
        abilities.put("Intelligence", 14);
        abilities.put("Charisma", 11);
    }


    public Map<String, Integer> getAbilities() {
        return abilities;
    }

    public int getMaxLifeDice() {
        return maxLifeDice;
    }

    public int getArmourBase(){
        return armourBase;
    }

    public void level1Power(Player player, Creature creature){
        int tryAttack = weapon.getAttack(player);
        System.out.printf("%s attacks %s (AC = %d) for %d\n", player.getName(), creature.getName(), creature.getAC(), tryAttack);
        if(tryAttack>=creature.getAC()){
            System.out.println("Hit succesful!");
            int damage = weapon.getDamage(player);
            creature.setCurrentHP(creature.getCurrentHP()-damage);
            System.out.printf("%s wounds %s (maxHP = %d) for %d: currentHP = %d\n", player.getName(), creature.getName(), creature.getMaxHP(), damage, creature.getCurrentHP());
        }else{
            System.out.println("Hit failed!");
        }
    }

    public void level3Power(Player player, Creature creature){
        if(getWeapon()==Weapon.BOW){
            System.out.println("You shoot your arrow with much more power then a normal shot.");
            int tryAttack = weapon.getAttack(player);
            System.out.printf("%s attacks %s (AC = %d) for %d\n", player.getName(), creature.getName(), creature.getAC(), tryAttack);
            if(tryAttack>=creature.getAC()){
                System.out.println("Hit succesful!");
                int damage = Dice.throwDice(12) + 2*((player.getAbilities().get("Dexterity")-10)/2);
                creature.setCurrentHP(creature.getCurrentHP()-damage);
                System.out.printf("%s wounds %s (maxHP = %d) for %d: currentHP = %d\n", player.getName(), creature.getName(), creature.getMaxHP(), damage, creature.getCurrentHP());
            }else{
                System.out.println("Hit failed!");
            }
        }else{
            System.out.println("You can only use this power with a Bow equipped!");
        }
    }

    public void level4Power(Player player, Creature creature){
        if(getWeapon()==Weapon.BOW){
            System.out.println("You loosely shoot an arrow that can not miss.");
            int damage = (weapon.getDamage(player))/2;
            System.out.println(damage);
            creature.setCurrentHP(creature.getCurrentHP()-damage);
            System.out.printf("%s wounds %s (maxHP = %d) for %d: currentHP = %d\n", player.getName(), creature.getName(), creature.getMaxHP(), damage, creature.getCurrentHP());
        }else{
            System.out.println("You can only use this power with a Bow equipped!");
        }
    }

    public void level5Power(Player player, Creature creature){
        if(getWeapon()==Weapon.BOW){
            if(creature instanceof Monster){
                int tryAttack = weapon.getAttack(player);
                System.out.printf("%s attacks %s (AC = %d) for %d\n", player.getName(), creature.getName(), creature.getAC(), tryAttack);
                if(tryAttack>=creature.getAC()){
                    System.out.println("Hit succesful!");
                    int damage = weapon.getDamage(player);
                    creature.setCurrentHP(creature.getCurrentHP()-damage);
                    System.out.printf("%s wounds %s (maxHP = %d) for %d: currentHP = %d\n", player.getName(), creature.getName(), creature.getMaxHP(), damage, creature.getCurrentHP());
                }else{
                    System.out.println("Hit failed!");
                }
            }
        }else{
            System.out.println("You can only use this power with a Bow equipped!");
        }
    }

    public void setWeapon(Weapon weapon){
        this.weapon = weapon;
    }

    @Override
    public Armour getArmour() {
        return armour;
    }

    @Override
    public void setArmour(Armour armour) {
        this.armour = armour;
    }

    @Override
    public Weapon getWeapon() {
        return weapon;
    }
}
