package be.dastudios.legendofthelamb.util;

import java.io.*;
import java.util.Scanner;

public class Keyboard {
    private static Scanner keyboard = new Scanner(System.in);

    public Keyboard() {
    }

    public static char readChar() {
        int input = 10;
        try {
            while (input <11){
                String s = keyboard.nextLine();
                if (!s.isEmpty()) {
                    input = s.toUpperCase().charAt(0);}}


        } catch (Exception ioe) {
            ioe.printStackTrace();
        }
        return (char) input;
    }

    public static int askForNumber(String promptMessage) {
     return askForNumber(promptMessage,Integer.MAX_VALUE);
    }

    public static int askForNumber(String promptMessage,int range) {
        System.out.print(promptMessage.isEmpty()?"":(promptMessage + ": "));
        if (keyboard == null) {
            keyboard = new Scanner(System.in);
        }
        int input=0;
        try {
            do {
                if (input != 0) {
                    System.out.println("please choose in range " + range);
                }
                    input = keyboard.nextInt();

            } while ((input>range||input<0));

        } catch (Exception ioe) {
            System.out.println("wrong input");
            System.out.println();
            keyboard.close();
            return askForNumber(promptMessage, range);
        }
        System.out.println();
        return input;
    }

    public static boolean askYesNo(String promptMessage) {
        System.out.print("\n" + promptMessage + "? yes / no ");
        char input=0;
        try {
            do {
                if (input != 0) {
                    System.out.println("please type yes or no");
                }
                String s = keyboard.nextLine();
                if (!s.isEmpty()) {
                    input = s.toLowerCase().charAt(0);
                }
            } while (input!='y'&&input!='n');
        } catch (Exception ioe) {
            System.out.println("wrong input");
        }
        System.out.println();
        return input=='y';
    }

    public static String readString(String promptMessage) {
        System.out.print(promptMessage + ": ");
        String input="";
        try {
            do {
                input = keyboard.nextLine();
            } while (input.isEmpty());

        } catch (Exception ioe) {
            System.out.println("wrong input");
        }
        System.out.println();
        return input;
    }

    public static <E> int chooseOption(E[] options,String promptMessage) {
        System.out.println(promptMessage);
        for (int i = 0; i < options.length; i++) {
            System.out.println("  " + (i + 1) + ". " + options[i]);
        }
        return askForNumber("choice", options.length)-1;
    }

    public static String writeOption(String[] options, String promptMessage) {
        System.out.println(promptMessage);
        for (String option : options) {
            System.out.println(option);
        }
        return readString("input");
    }
}
