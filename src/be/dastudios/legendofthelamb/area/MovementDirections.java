package be.dastudios.legendofthelamb.area;

import java.util.Arrays;

public enum MovementDirections {
    NORTH {
        @Override
        public int[] calculateDirectionCoordinate(int[] direction) {
            return new int[]{direction[0] - 1, direction[1]};
        }
    },
    SOUTH{
        @Override
        public int[] calculateDirectionCoordinate(int[] direction) {
            return new int[]{direction[0] + 1, direction[1]};
        }
    },
    WEST{
        @Override
        public int[] calculateDirectionCoordinate(int[] direction) {
            return new int[]{direction[0] , direction[1]- 1};
        }
    },
    EAST{
        @Override
        public int[] calculateDirectionCoordinate(int[] direction) {
            return new int[]{direction[0], direction[1] + 1};
        }
    },
    ;

    public int[] calculateDirectionCoordinate(int[] direction) {
        return new int[]{0, 0};
    }
}

