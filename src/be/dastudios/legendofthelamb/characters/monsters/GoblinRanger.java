package be.dastudios.legendofthelamb.characters.monsters;

import be.dastudios.legendofthelamb.characters.Player;
import be.dastudios.legendofthelamb.util.Dice;

public class GoblinRanger implements Monster{
    private int AC = 15;
    private int maxHP = 12;
    private int currentHP = 12;
    private CR cr = CR.CR1;
    private int attack;
    private int damage;
    private String name = "Goblin Ranger";

    @Override
    public String getName() {
        return name;
    }

    public int getAC() {
        return AC;
    }

    public int getMaxHP() {
        return maxHP;
    }

    public int getCurrentHP() {
        return currentHP;
    }

    public void setCurrentHP(int currentHP) {
        if(currentHP>maxHP){
            this.currentHP = maxHP;
        }else{
            this.currentHP = currentHP;
        }

    }

    public int getCR() {
        return 1;
    }
    public int  getXPfromCR() {
        return cr.getXp();
    }

    public int getAttack() {
        return Dice.throwDice(20) + 4;
    }

    public int getDamage() {
        return Dice.throwDice(6) + 3;
    }

    @Override
    public String toString(){
        return String.format("GoblinRanger{ AC = %d, maxHP = %d, currentHP = %d, CR = %d }", AC, maxHP, currentHP, getCR());
    }

}
